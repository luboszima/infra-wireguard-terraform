#!/bin/sh

set -e

host_file="$1_hosts"

echo "waiting to fully applied cloud-init"
sleep 6m

# https://markontech.com/linux/install-wireguard-vpn-server-with-docker/
echo "Ansible playbook for install docker daemon"
echo "$1 ansible_user=$2 ansible_port=$3 ansible_ssh_common_args='-o StrictHostKeyChecking=no'" > "$host_file"

echo "Ansible playbook for setup wireguard"
ansible-playbook -v -i "$host_file" ansible/setup-wireguard.yml --extra-vars "droplet_domain=${4}"
rm -rf "$host_file"

# get config from server
ssh -p ${3} ${2}@${1} cat /home/$2/wireguard/config/peer1/peer1.conf > wireguard-vpn.conf
