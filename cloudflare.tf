# Look for a single zone that you know exists using an exact match.
data "cloudflare_zone" "maindomain" {
  name = var.domainname
}

# Add a record to the domain
resource "cloudflare_record" "luboszima_space" {
  zone_id = data.cloudflare_zone.maindomain.id
  name    = digitalocean_droplet.vpn.name
  value   = digitalocean_droplet.vpn.ipv4_address
  type    = "A"
}
