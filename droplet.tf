# create cloud init template
data "template_file" "cloud-init-yaml" {
  template = file("${path.module}/cloud-init/cloud-init.yaml")
  vars = {
    timezone = var.machine_timezone
    user_name = var.user_name
    default_ssh_port = var.default_ssh_port
    init_ssh_public_key = var.ssh_public_key
  }
}

# Create a new tag
resource "digitalocean_tag" "vpn" {
  name = "vpn"
}

# Create a new tag
resource "digitalocean_tag" "docker" {
  name = "docker"
}

# Create a new Web Droplet in the fra1 region
resource "digitalocean_droplet" "vpn" {
  image  = "ubuntu-20-04-x64"
  name   = "${var.machine_name}"
  region = var.digitalocean_droplet_region
  size   = var.digitalocean_droplet_size
  tags   = [digitalocean_tag.vpn.id, digitalocean_tag.docker.id]
  user_data  = data.template_file.cloud-init-yaml.rendered

  # after
  provisioner "local-exec" {
    command = "sh ./apply-ansible.sh ${self.ipv4_address} ${var.user_name} ${var.default_ssh_port} ${var.machine_name}.${var.domainname}"
  }
}
