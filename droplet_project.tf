# create new project
resource "digitalocean_project" "infra" {
  name        = "Infra"
  description = "A project to represent infra resources."
  purpose     = "Infrastructure"
  environment = "Development"
}

# add machine to new project
resource "digitalocean_project_resources" "infra" {
  project = digitalocean_project.infra.id
  resources = [digitalocean_droplet.vpn.urn]
}
