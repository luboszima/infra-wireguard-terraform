output "ip" {
  value = digitalocean_droplet.vpn.ipv4_address
}

output "server_name" {
  value = digitalocean_droplet.vpn.name
}

output "machine_name" {
  value = var.machine_name
}

output "domain" {
  value = data.cloudflare_zone.maindomain.name
}

output "ssh_port" {
  value = var.default_ssh_port
}

output "user" {
  value = var.user_name
}


#data "digitalocean_images" "all_images" {
#  filter {
#    key    = "regions"
#    values = ["fra1"]
#  }
#}
#
#output "digitalocean_all_images" {
#  value = data.digitalocean_images.all_images.images[*].slug
#}
#
#
#data "digitalocean_regions" "available" {
#  filter {
#    key    = "available"
#    values = ["true"]
#  }
#}
#
#output "digitalocean_all_regions" {
#  value = data.digitalocean_regions.available.regions[*].name
#}
