terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "2.18.0"
    }
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "3.11.0"
    }
  }
}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.digitalocean_token
}

# Configure the Cloudflare Provider
provider "cloudflare" {
  # Configuration options
  api_token = var.cloudflare_api_token
}