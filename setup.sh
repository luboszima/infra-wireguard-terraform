#!/usr/bin/env bash

terraform init
terraform apply -auto-approve

if [ -f wireguard-vpn.conf ]; then
  echo "Wireguard setup client"
  cat wireguard-vpn.conf | egrep -v DNS > wireguard-vpn.conf_tmp
  sudo mv wireguard-vpn.conf_tmp /etc/wireguard/wg0.conf
  sudo chmod 600 /etc/wireguard/wg0.conf

  rm -fr wireguard-vpn.conf

  # Disconnect
  sudo wg-quick down wg0

  # Connect
  sudo wg-quick up wg0

  # Disconnect
  # sudo wg-quick down wg0
else
    echo "Wireguard config not found"
fi
