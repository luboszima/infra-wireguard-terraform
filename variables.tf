variable "machine_instances" {
  default = 1
}

variable "ssh_public_key" {
  type = string
  default = ""
  description = "Public ssh key for login into server as a created user."
}

variable "user_name" {
  type = string
  default = "devops"
  description = "First user created on server."
}

variable "default_ssh_port" {
  type = string
  default = "22"
  description = "Port for ssh connection."
}

variable "cloudflare_api_token" {
  type = string
  description = "Cloudflare api token."
}

variable "digitalocean_token" {
  type = string
  description = "Digitalocean api token."
}

variable "machine_name" {
  type = string
  default = "vpn"
  description = "Droplet machine name."
}

variable "machine_timezone" {
  type = string
  default = "Europe/Prague"
  description = "Server timezone."
}

variable "domainname" {
  type = string
  description = "Domain name."
}

variable "digitalocean_droplet_region" {
  type = string
  default = "nyc1"
  description = "Droplet region."
}

variable "digitalocean_droplet_size" {
  type = string
  default = "s-1vcpu-1gb"
  description = "Droplet size."
}


